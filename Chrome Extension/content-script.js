function insert_tags(tags) {
  let tags_html = tags.map(tag => {
    return '<li><a>' + tag + "</a></li>";
  });
  tags_html='<ul class="list-inline">'+tags_html+'</ul>'

  const el = document.querySelector("#tag_extractor_content_div");

  if (el !== null) {
    //console.log('update tags block')
    el.innerHTML = tags_html;
  } else {
    const el = document.querySelector(
      ".title.style-scope.ytd-video-primary-info-renderer"
    );

    if (el !== null) {
      //console.log('create tags block')
      var div = document.createElement("div");
      div.setAttribute("id", "tag_extractor_content_div");
      div.classList.add("tags");
      div.innerHTML = tags_html;
      el.after(div);
    }
  }
}

function videoUpdated() {
  const videoId = new URL(location.href).searchParams.get("v");
  //console.log("Video updated", videoId);

  //Remove block content from previous video
  const el = document.querySelector("#tag_extractor_content_div");
  if (el !== null) el.innerHTML = '';


  chrome.runtime.sendMessage(
    { action: "get-tags", videoId: videoId },
    function(response) {
      //console.log("TAGS", response.tags);
      insert_tags(response.tags);
    }
  );
}

document.addEventListener("yt-page-data-updated", videoUpdated);
