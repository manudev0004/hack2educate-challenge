//console.log('background.run')
async function getTags(videoId) {
  //console.log('getTags called',videoId)
  const response = await fetch("https://www.youtube.com/watch?v=".concat(videoId));
  const body = await response.text();
  //console.log(body)
  const match_title=body.match(/"title" content="([^"]*)"/)
  // const match_result=body.match(/"keywords" content="([^"]*)"/)

  //console.log(match_result[1])
  let title = match_title;
  // let tags = match_result[1].split(', ');
  let tag
  // let data = {Title: title};

  // fetch('http://manudev0004.pythonanywhere.com//predict', {
  //   method:"GET",
  //   body: JSON.stringify(data)
  //   }).then(result => {
  //       // do something with the result
  //       console.log("Completed with result:", result);
  //   }).catch(err => {
  //       // if any error occured, then catch it here
  //       console.error(err);
  //   });
  // //console.log('getTags result',tags)

  const form = new FormData();
  form.append("Title", "Why can't you go faster than light  ");

  const options = {
    method: 'POST',
    body : form    
  };

  // fetch('http://manudev0004.pythonanywhere.com/predict', options)
  //   .then(response => response.text())
  //   .then(response => console.log(response))
  //   .catch(err => console.error(err))
  //   // tags = response
  //   ;
  const res = await fetch('http://manudev0004.pythonanywhere.com/predict', options)
  tag = await res.text();
  let tags = [tag];
  // let tags = ['Educationallly'];
  return tags
}

//Listen to messages
chrome.runtime.onMessage.addListener(
  function(request, sender, sendResponse) {
    if (request.action === "get-tags") {
      getTags(request.videoId).then((tags) => {
        sendResponse({tags: tags});
      })
      }

  //Need this to indicate asyn response
  return true;
  }
);

