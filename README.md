## **Team Radian** 😎

- Rohan Babbar - rohanbabbar0408@gmail.com

- Manu Dev - manudev0004@gmail.com

- Shivam Kumar - shivamkumardss2018@gmail.com

- Abhinav Kumar - dead.ground.75000@gmail.com

# Theme 👇
## Filter and categorise Youtube videos
Categorise youtube videos to Educational or not. Then further divide into the varios  sub-category of Educational video (eg- Coding, Music etc.)

Also figure out which BeyondExams category would this video be most suitable for.
"Build a chrome extension that calculates how much time you spent learning about which domains.
How much time you spend on non-educational videos."


# Solution to our Problem👨‍💻✔️
We're going to use Python and SK-learn to create an AI model. We will use different YouTube video data to train them and predict whether the video is educative or not. Further we can make a website where we will use this in the back-end to predict a video. User can enter the URL to see the output.

Also, We have to learn about YOUtube APIs because with the help of YouTube APIs we can easily extract the data and make a match with our database to cross check the tites, commments, tags etc by which we can easily find out that the YT video is educational or non educational,and at last we have to make a localhost server and connect it to our extension or website
to predict YT video as educational or non educational.

# Code editors

Basically, We are using VS code as code editor, Anaconda which is a open source package for machine learning and deap learning and Jupyter Notebook a web based interactive computing platform. 

# Programming Languages
1. Python
2. Javascript
3. CSS

# Some open-source tools 
For built-in development server and a fast debugger.Fast, powerful, flexible and easy to use open source data analysis and manipulation tool, built on top of the Python programming language. which helps us to run our code linked with the database more efficiently.
1. Flask 
2. Sk learn
3. Pandas
4. Anaconda
5. Jupyter Notebook
6. Pythonanywhere

 

# Our Final Demo 👨‍🔬

We made a user-friendly website where the user will enter video url to see the result predicted using our AI model. We also made a chrome extension which can be installed on browser. This enables the prediction to be made directly below the youtube title.

Demo Link - https://youtu.be/aAKAJy_kW1M









